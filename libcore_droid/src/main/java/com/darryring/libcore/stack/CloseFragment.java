package com.darryring.libcore.stack;

/**
 */
public interface CloseFragment {
    void close(StackFragment fragment);

    void show(StackFragment fragment);
}
