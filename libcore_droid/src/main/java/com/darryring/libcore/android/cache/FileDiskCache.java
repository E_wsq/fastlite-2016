package com.darryring.libcore.android.cache;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.darryring.libcore.Fast;
import com.darryring.libcore.cache.DiskCache;
import com.jakewharton.disklrucache.DiskLruCache;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hljdrl on 16/3/5.
 */
public class FileDiskCache implements DiskCache {

    private Context mContext;
    DiskLruCache mDiskLruCache;
    private String TAG="FileDiskCache";

    /**
     * @param context
     */
    public FileDiskCache(Context context){
        mContext = context;
        String file = getDiskCacheDirPath(mContext,"diskCache");
        int appVersion = getAppVersion(mContext);
        try {
            mDiskLruCache = DiskLruCache.open(new File(file), appVersion, 1, 50 * 1024 * 1024);
        } catch (IOException e) {
            e.printStackTrace();
            Fast.logger.i(TAG,"DiskLruFileCache()----->NEW....ERROR");
        }
        Fast.logger.i(TAG,"DiskLruFileCache()----->NEW....");
        Fast.logger.i(TAG,"DiskLruFileCache()----->FILE--->"+mDiskLruCache.getDirectory());

    }

    /**
     * @param key
     * @return
     */
    String hashKeyForDisk(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    /**
     * @param bytes
     * @return
     */
    String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    @Override
    public void saveString(String key, String data) {
        String enkey = hashKeyForDisk(key);
        try {
            DiskLruCache.Editor editor = mDiskLruCache.edit(enkey);
            if (editor != null) {
                OutputStream outputStream = editor.newOutputStream(0);
                if (data!=null) {
                    IOUtils.write(data,outputStream);
                    if(outputStream!=null){
                        outputStream.close();
                    }
                    editor.commit();
                } else {
                    editor.abort();
                }
            }
            mDiskLruCache.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String readString(String key) {
        String enkey = hashKeyForDisk(key);
        String _value=null;
        try {
            DiskLruCache.Snapshot snapShot = mDiskLruCache.get(enkey);
            if (snapShot != null) {
                InputStream is = snapShot.getInputStream(0);
                _value = IOUtils.toString(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return _value;
    }

    @Override
    public boolean remove(String key) {
        String enkey = hashKeyForDisk(key);
        try {
            mDiskLruCache.remove(enkey);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @return
     */
    @Override
    public long size() {
        return mDiskLruCache.size();
    }

    /**
     * @param context
     * @return
     */
     int getAppVersion(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    /**
     * @param context
     * @param uniqueName
     * @return
     */
    String getDiskCacheDirPath(Context context, String uniqueName) {
        String cachePath = null;
        String path = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
            if (context.getExternalCacheDir().getPath() != null) {
                cachePath = context.getExternalCacheDir().getPath();
            }
        } else {
            if (context.getCacheDir().getPath() != null) {
                cachePath = context.getCacheDir().getPath();
            }else{
                cachePath = context.getCacheDir().toString();
            }
        }
        if (cachePath != null) {
            path = cachePath + File.separator + uniqueName + File.separator;
        }
        return path;
    }
}
