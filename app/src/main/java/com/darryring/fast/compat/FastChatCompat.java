package com.darryring.fast.compat;

import com.darryring.libchat.FastChat;
import com.libchat.easemob.EasemobChat;

/**
 * Created by hljdrl on 16/7/12.
 */
public class FastChatCompat {

    /**
     * @return
     */
    public static final FastChat getInstance(){
        return FastChat.getInstance(EasemobChat.class);
    }
}
