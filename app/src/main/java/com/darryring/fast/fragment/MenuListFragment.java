package com.darryring.fast.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.activitys.ThemeActivity;
import com.darryring.fast.adapter.MenuAdapter;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.libmodel.entity.MenuEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/2.
 */
public class MenuListFragment extends BaseFragment implements AdapterView.OnItemClickListener{

    ListView mListview;
    List<MenuEntity> menus = new ArrayList<MenuEntity>();
    ArrayAdapter<MenuEntity> mArrayAdapter;
    public MenuListFragment(){
    }
    @SuppressLint("ValidFragment")
    public MenuListFragment(String _title){
        setTitle(_title);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_menulist,container,false);
        return _view;
    }
    List<MenuEntity> datas(){
        List<MenuEntity> _list = new ArrayList<MenuEntity>();
        MenuEntity _m1 = new MenuEntity();
        _m1.setName("主题");
        Intent _intent = new Intent(getActivity(), ThemeActivity.class);
        _m1.setIntent(_intent);
        _list.add(_m1);

        return _list;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View _view = getView();
        mListview = (ListView) _view.findViewById(R.id.listview_menu);
        menus.addAll(datas());
        mArrayAdapter = new MenuAdapter(getActivity(),menus);
        TypedArray ta = getActivity().getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
        int _defaultColor = Color.TRANSPARENT;
        int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
        ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
        Drawable _drawable = mListview.getSelector();
        Drawable _tintDrawable =  DrawableUtil.tintDrawable(_drawable,colorStateList);
        mListview.setSelector(_tintDrawable);
        //----
        ta.recycle();
        mListview.setAdapter(mArrayAdapter);
        mListview.setOnItemClickListener(this);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MenuEntity _entity = mArrayAdapter.getItem(position);
        Intent _intent = _entity.getIntent();
        startActivity(_intent);
        getActivity().finish();
    }
}
