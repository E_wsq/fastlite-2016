package com.darryring.libchat.listener;

/**
 * Created by hljdrl on 16/7/12.
 */
public interface FastChatListener {

    /**
     * 聊天列表CALL
     */
    public static final int TYPE_SESSION_CALL = 10;

    /**
     * 通讯录CALL
     */
    public static final int TYPE_CONTACT_CALL = 20;
    /**
     * 个人资料CALL
     */
    public static final int TYPE_VCARD_CALL = 30;

    void callChat(int typeCall);
}
