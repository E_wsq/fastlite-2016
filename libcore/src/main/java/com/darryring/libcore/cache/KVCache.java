package com.darryring.libcore.cache;


/**
 * Created by hljdrl on 15/12/11.
 */
public interface KVCache  {

    /**
     * @param k
     * @param v
     */
    public void saveToFileCache(String k,String v);
    /**
     * @param k
     * @param obj
     */
    public void saveToMemory(String k,Object obj);
    /**
     * @param k
     * @return
     */
    public Object readFromMemory(String k);

    /**
     * @param k
     */
    public void deleteToMemory(String k);

    /**
     * @param k
     * @return
     */
    public String readFromFileCache(String k);

    /**
     * @param k
     */
    public void deleteToFileCache(String k);


}
